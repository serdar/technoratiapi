using System;
using System.Net;
using System.Web;
using System.IO;

/*
    Serdar Kilic, http://kilic.net/, 2003-05-13
    
    v0.2 - 2003-05-15
        - Removed Parameters class
        - Added parameterized constructors
        - Added default values to Start and Version
        - Amalgamated methods to GetData
        - Added enum for operations that could be performed
    v0.1 - 2003-05-13
        - initial implementation

    C# Implementation of Technorati API as outlined at
    http://www.sifry.com/alerts/archives/000288.html

*/

namespace net.kilic.Technorati
{  
    public enum Operation
    {
        Cosmos = 0,
        BlogInfo,
        Outbound
    }
    
    public class TechnoratiApi
    {
        private const string apiUrl = @"http://api.technorati.com/";
        private string queryString;
        private string type;
        private string start = "0";
        private string version = "0.9";
        private string key;
        private string url;
            
        public string Type
        {
            get {
                return type;
            }
            set {
                type = value;
            }
        }
        
        public string Start
        {
            get {
                return start;
            }
            set {
                start = value;
            }            
        }
        
        public string Version
        {
            get {
                return version;
            }
            set {
                version = value;
            }            
        }
        
        public string Key
        {
            get {
                return key;
            }
            set {
                key = value;
            }
        }
        
        public string Url
        {
            get {
                return url;
            }
            set {
                url = value;
            }
        }  
                
        public TechnoratiApi(string url, string key)
        {          
            this.url = url;
            this.key = key;
        }

        public TechnoratiApi(string url, string key, string version, string start, string type)
        {            
            this.url     = url;
            this.key     = key;
            this.version = version;
            this.start   = start;
            this.type    = type;
        }
        
        private void BuildUrl ()
        {
            if (!Empty(url))     queryString += "url=" + HttpUtility.UrlEncode(url) + "&";
            if (!Empty(key))     queryString += "key=" + HttpUtility.UrlEncode(key) + "&";
            if (!Empty(version)) queryString += "version=" + HttpUtility.UrlEncode(version) + "&";
            if (!Empty(type))    queryString += "type=" + HttpUtility.UrlEncode(type) + "&";
            if (!Empty(start))   queryString += "start=" + HttpUtility.UrlEncode(start);
        }
        
        public string GetData (Operation operation)
        {
            string output = null;
            
            BuildUrl();
            
            Uri cosmosUri = new Uri (apiUrl + operation.ToString().ToLower() + "?" + queryString);

            try {

                WebRequest request = WebRequest.Create (cosmosUri);
                WebResponse response = request.GetResponse();
                
                StreamReader stream = new StreamReader(response.GetResponseStream());
                
                output = stream.ReadToEnd();            
            }
            
            catch (WebException ex) {throw ex;}
 
            return output;
        }
        
        static bool Empty(string s) { return s == null || s.Length == 0; }
    }
    
    public class Test
    {
        public static void Main (string [] args)
        {               
            string url = null;
            if (args.Length != 1)
            {
              Console.WriteLine ("Provide url");
              return;
            } else {
              url = args[0];
            }
            string key = "";
            
            TechnoratiApi tapi = new TechnoratiApi(url, key);
            
            Console.WriteLine("== Cosmos ==");
	    Console.WriteLine(tapi.GetData(Operation.Cosmos));
            Console.WriteLine("== BlogInfo ==");
	    Console.WriteLine(tapi.GetData(Operation.BlogInfo));
            Console.WriteLine("== Outbound ==");
            Console.WriteLine(tapi.GetData(Operation.Outbound));
        }
    }
}