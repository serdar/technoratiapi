using System;
using System.IO;
using System.Net;

public class Cosmos
{
  public string _tapiKey;
  public string _searchUrl;
  public int _limit;
  public int _start;
  public bool _current;
  public Format _format;
  public HttpMethod _httpMethod;
  
  public enum Type
  {
   Link,
   Weblog
  }

  
  

  public enum Format
  {
   Xml,
   Rss
  }

  public enum HttpMethod
  {
   HttpGet,
   HttpPost
  }
  
  public void Find ()
  {
    string url = "http://api.technorati.com/cosmos?key=" + _tapiKey + "&url=" + _searchUrl;
    HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
    HttpWebResponse wrsp = (HttpWebResponse)wr.GetResponse();
    
    using (StreamWriter sw = File.CreateText("temp.xml"))
    {
      using (StreamReader sr = new StreamReader(wrsp.GetResponseStream()))
      {
        sw.Write (sr.ReadToEnd());
      }
      
      sw.Close();
    }
    
  }
}

public class Test
{
 public static void Main ()
 {
    Cosmos c = new Cosmos ();
    c._tapiKey = "";
    c._searchUrl = "http://weblog.kilic.net/";
    c._format = Cosmos.Format.Xml;
    c._httpMethod = Cosmos.HttpMethod.HttpGet;
    c.Find();
 }
}